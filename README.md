Working Version MTFSK v0.1
==========================


How to Add File to TrillBPP

Add Files
```ruby 
# Add Files
Right click on project -> Add Files select Others -> Select Files from directory.

# Add Files to configrations ( to make sure all files are copied inside build/include folder).s
Select Project -> Build Phase -> Copy Files -> Add All header files inm this.
```

Make sure while compiling You select libtrillBPP_static from project compile directory, right to > (play bvutton )


For Logging: 

1. Make sure the file type in Identity and Type is Objective-C++ Source.
2. Add Header <Foundation/Foundation.h>
3. For Logging
```ruby
	NSLog(@"Logging from Static Library");
	or
	NSLog(@"Logging from Static Library %d", 12);
	or 
	NSLog(@"Logging from Static Library %f", 15.0);
```