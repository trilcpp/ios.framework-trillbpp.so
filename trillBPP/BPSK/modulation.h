/*
 * modulation.h
 *
 *  Created on: 12-April-2019
 *      Author: narasimha vineeth
 */

#ifndef _MODULATION_H_
#define _MODULATION_H_

//#include <fftw3.h>

#include <trillBPP/transforms.h>
#include <trillBPP/miscfunc.h>		//For xcorr - levels2bit,cvec, conj
////#include<android/log.h>



using namespace std;
using namespace trill;


class modulation {
private:
	void xcorr(const vec &x, const vec &y, cvec &out, const int max_lag = -1, const std::string scaleopt = "none",
	           bool autoflag = true);
public:
	vec filter_one_sided(cvec FFT_input, int fStart,int micstate);
	vec filter_one_sided_N(cvec FFT_input, int fStart,int micstate);
	vec convolve_using_fft(cvec dataInput, vec taps1,int micstate);
	void getWelchPSD(vec inputData, double output,int nperseg, int fs,int micstate);
	vec getWelchPSD_trigger(vec inputData, double output,int nperseg, int fs,int micstate);
	cvec convolve_freq_domain(const cvec data_bits, vec rrc_wave);

	//Overloaded functions- Auto correlate and Cross-correlate
	vec xcorr(const vec &x, const vec &y, const int max_lag = -1, const std::string scaleopt = "none");
	vec xcorr(const vec &x, const int max_lag = -1, const std::string scaleopt = "none");


//FFT
//	cvec fft(cvec input);

};



#endif /* MODULATION_H_ */
