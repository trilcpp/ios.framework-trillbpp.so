/*
 * correlate.h
 *
 *  Created on: Dec 21, 2016
 *      Author: rajanya
 *      @description :
 */

#ifndef CORRELATE_H_
#define CORRELATE_H_


#include <trillBPP/MTFSK.h>
#include <trillBPP/vec.h>

using namespace trill;

class Correlate {
	vec syncWave;
public:
	Correlate();
    vec apply(vec data, vec sync, const std::string scale="none" );
    vec baseband_corr(vec data, int fc=17000, int phi=0);
};

#endif /* CORRELATE_H_ */
