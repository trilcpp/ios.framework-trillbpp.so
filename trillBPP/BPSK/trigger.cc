


/*
 * trigger.cc
 *
 *  Created on: May 27, 2017
 *      Author: Saurabh
 */


#include <trillBPP/trigger.h>
#include <trillBPP/modulation.h>

//#include <cstdlib>
//#include <android/log.h>

#define Fs 44100
#define G .3
#define H 0.5


int getIndexForFreq(int freq, int len, int fs) {
	int index = ( ( len / (float) fs ) * freq);
	return index;
}

vec getFilterdAmplitude(vec amp, int size) {
	int sizeAmp = amp.size();
	int freqIndex14k = getIndexForFreq(14000, size, Fs);
	vec zeroArray = amp.get(0, freqIndex14k-1);
	zeroArray.zeros();
	return concat(zeroArray, amp.get(freqIndex14k, sizeAmp-1));
}

double getAverage(vec Vec, int start, int end) {
	double avg = 0;
	double size = end - start;
	for (int i = start; i<end; i++) {
		avg += Vec[i];
	}
	return avg / size;
}


// @todo Diversion to Deviation.
vec getDiversionSquare(vec amp, double mean, int Index14K) {
	vec diversionSquare;
	int counter = 0;

	for (int i=Index14K; i<amp.size(); i++) {
		double dataSelected = amp[i]-mean;
		dataSelected = dataSelected * dataSelected;
		diversionSquare.ins(counter, dataSelected);
		counter = counter + 1;
	}

	return diversionSquare;
}

std::pair<double, double> getGammaValue(vec diversionValuesSquare, double mean) {

	double sigmaSq = getAverage(
			diversionValuesSquare,
			0,
			diversionValuesSquare.size()
			);

	double sigma = pow(sigmaSq, 0.5);
	double gamma = ( (G * sigmaSq) + (H * sigma) ) / mean;

	return make_pair(gamma, sigmaSq);

}

std::pair<double, double> checkCondition(vec amp, double gamma, double mean){
	int sizeAmp = amp.size();

	int freq14KIndex = getIndexForFreq(15000, sizeAmp, 22050) + 1;
	int freq16KIndex1 = getIndexForFreq(16000, sizeAmp, 22050) + 1;

	int freq16KIndex2 = getIndexForFreq(16500, sizeAmp, 22050) + 1;
	int freq18KIndex = getIndexForFreq(17500, sizeAmp, 22050) + 1;

	double gamma14K = 0.1;
	int counter14 = 0;
	double gamma16K = 0.1;
	int counter16 = 0;
	vec top100_15 = amp.get(freq14KIndex,freq16KIndex1);
	for( int i =freq14KIndex; i < freq18KIndex; i++) {
		if(i < freq16KIndex1 ) {
			if (abs(mean - amp[i]) > gamma) {
				gamma14K += amp[i];
				counter14 += 1;

			}
		}
		else if( i > freq16KIndex2) {
			if (abs(mean - amp[i]) > gamma) {
				gamma16K += amp[i];
				counter16 += 1;
			}
		}
	}

	if (gamma14K > 0.1) {
		gamma14K = gamma14K / counter14;
	}
	if (gamma16K > 0.1) {
		gamma16K = gamma16K / counter16;
	}

	double ratio = gamma16K / gamma14K;
	return make_pair(gamma14K, gamma16K);
}

std::pair<double, double> checkCondition_data(vec amp, double gamma, double mean){
	int sizeAmp = amp.size();

	int freq14KIndex = getIndexForFreq(14000, sizeAmp, 22050) + 1;
	int freq16KIndex1 = getIndexForFreq(16000, sizeAmp, 22050) + 1;

	int freq16KIndex2 = getIndexForFreq(17500, sizeAmp, 22050) + 1;
	int freq18KIndex = getIndexForFreq(19500, sizeAmp, 22050) + 1;

	double gamma14K = 0.1;
	int counter14 = 0;
	double gamma18K = 0.1;
	int counter18 = 0;
	vec top100_15 = amp.get(freq14KIndex,freq16KIndex1);
	for( int i =freq14KIndex; i < freq18KIndex; i++) {
		if(i < freq16KIndex1 ) {
			if (abs(mean - amp[i]) > gamma) {
				gamma14K += amp[i];
				counter14 += 1;

			}
		}
		else if( i > freq16KIndex2) {
			if (abs(mean - amp[i]) > gamma) {
				gamma18K += amp[i];
				counter18 += 1;
			}
		}
	}

	if (gamma14K > 0.1) {
		gamma14K = gamma14K / counter14;
	}
	if (gamma18K > 0.1) {
		gamma18K = gamma18K / counter18;
	}

	double ratio = gamma18K / gamma14K;
	return make_pair(gamma14K, gamma18K);
}

float sumVector(vec data, int starting_point, int ending_point) {
	float result = 0;
	for (int i=starting_point; i < ending_point; i++) {
		result += (data[i]*data[i]); //Amplitude square for getting Power
	}
	return result;
}


double Trigger::isFound(vec inputchunk, bool solo_correlation, int &TriggerIndex) {
	double gamma_result;
	for(int i=0;i<4;i++)
	{
		vec input1 = inputchunk.get(i*1024,((i+1)*1024) -1);
		TriggerIndex = i+1;
		cvec input;
		if(solo_correlation==true)
			input = fft_REAL(input1);
		vec mag = abs(input); 		//Note : The input cvec is Computed FFT.
		int data_size = mag.size();
		vec amp = mag.get(0, (data_size/2)-1); // discarding mirror image

		vec filteredArray = getFilterdAmplitude(amp, data_size); // get amplitude between 14k to 22050.

		vec chunkedData = filteredArray;

		int freq15Kindex = getIndexForFreq(15000, chunkedData.size(), Fs/2); // find index of 14000
		double mean = getAverage(
				chunkedData,
				freq15Kindex,
				chunkedData.size()
				);
		vec diversionValuesSquare = getDiversionSquare(chunkedData, mean, freq15Kindex);
		std::pair<double, double> resultGamma = getGammaValue(diversionValuesSquare, mean);
		double gammaValue = resultGamma.first;
		double sigmaSqrValue = resultGamma.second;
		std::pair<double, double> result = checkCondition(chunkedData, gammaValue, mean);
		double sigma = pow(sigmaSqrValue, 0.5);
		gamma_result = result.second/result.first;
		if(gamma_result > 65)
			break;
	}
	return gamma_result;
}

double Trigger::isFound_data(vec inputchunk, bool solo_correlation) {
	int fs = 44100;
	double output;
	int micstate = 0;
	int nperseg = 1024;
	modulation mod;
	vec logOut = mod.getWelchPSD_trigger(inputchunk,output,nperseg,fs,micstate);
        int index17_5K = getIndexForFreq(17500, logOut.size(), Fs/2);  
        int index19_5K = getIndexForFreq(19500, logOut.size(), Fs/2);  
	logOut = logOut.get(index17_5K,index19_5K);
	output = max(logOut);
	return output;
}





