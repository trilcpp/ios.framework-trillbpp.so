/*
 * MTFSK.h
 *
 *  Created on: 12-April-2019
 *      Author: narasimha vineeth
 */

#ifndef TRILLBPP_MTFSK_MTFSK_H_
#define TRILLBPP_MTFSK_MTFSK_H_




//remove
#include<iostream>
#include<iomanip>
using namespace std;

#include <trillBPP/vec.h>
#include <trillBPP/modulation.h>
#include <trillBPP/correlate.h>



//#include<android/log.h>


//...Configuration
#define FS 44100
#define PEAK_CORR_THRESHOLD 100000.0
#define TB_DURATION 172
#define Fsync 17000
#define Fdata 17500
#define AMP_DATA 1
#define roll_off 0.25
#define SAMPLES_PER_SYMBOL 408
#define FRAMES_IN_RRC 400   //it is the 'frames' variable name in Python
#define OV_FACT 250
#define SPAN 1
#define PI 3.1415926535
//#define SAMPLE_SYMBOL_DEMOD 408


#define DIST_BW_PEAKS 12520
#define DIST_PEAK_DATA 4280	//distance_of_data

using namespace trill;

class MTFSK{
	vec rrc_wave;

public:
	MTFSK();
	//Note: ov_fact was written as upsampling factor in Python Code
	cvec demodulate_MTFSK(vec input, int fc=18000, int ov_fact=40, int span=10, int phi=0);
	vec demodulate_MTFSK_8(vec input, bool tempstate,int micstate,int fc=Fdata, int ov_fact=OV_FACT, int span=SPAN, int phi=0);
	int demodulate_MTFSK_findpeak(vec input, int number_for_peak,int ChunkSize,double peak_ratio,int micstate);
	int demodulate_MTFSK_findpeak_New(vec input, int number_for_peak,int ChunkSize,double peak_ratio,int micstate,int index);
	vec trillDecoder_MTFSK_8(vec input,int peak,int no_of_bits, int ChunkSize,double peak_ratio,int micstate);
	vec modulate_MTFSK(const char* string, int state);
	void plotData(vec plotVector);
	string decode_bytes(vec result);
	string decode_bits(vec result);
	vec modulate_rawdata(vec maindatabits);

	//Does Peak detection and then sends for demodulation: returns final_bits
	//Note Input format : Trigger, + 24576 -> Peaks at x distance , + 12280 -> data start , where data length is 8038
};


#endif /* TRILLBPP_MTFSK_MTFSK_H_ */
