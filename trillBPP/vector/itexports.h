/*
 * itexports.h
 *
 *  Created on: 29-Aug-2017
 *      Author: shivujagga
 */

#ifndef TRILLBPP_ITEXPORTS_H_
#define TRILLBPP_ITEXPORTS_H_


/*defined from cmake when using the shared version of IT++ library*/
#define ITPP_SHARED_LIB

/*needed to export shared library symbols on Windows*/
#if defined(ITPP_SHARED_LIB) && defined(_MSC_VER)
  #ifndef ITPP_EXPORT
    #if defined(itpp_EXPORTS) || defined(itpp_debug_EXPORTS) /*automatically defined by cmake*/
      #define ITPP_EXPORT __declspec(dllexport)
    #else
      #define ITPP_EXPORT __declspec(dllimport)
    #endif
  #endif
#endif

#if (__GNUC__ >= 4) /*UNIX*/
  #ifndef ITPP_EXPORT_TEMPLATE
    #define ITPP_EXPORT_TEMPLATE extern
  #endif
#endif

#ifndef ITPP_EXPORT
  #define ITPP_EXPORT
#endif
#ifndef ITPP_EXPORT_TEMPLATE
  #define ITPP_EXPORT_TEMPLATE
#endif

#endif
