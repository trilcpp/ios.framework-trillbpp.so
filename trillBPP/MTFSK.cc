/*
 * MTFSK.cc
 *
 *  Created on: 12-April-2019
 *      Author: narasimha vineeth
 */
#include <trillBPP/MTFSK.h>
#include <trillBPP/helper.h>
#include <math.h>
#include <trillBPP/array.h>
#include <trillBPP/miscfunc.h>
#include <trillBPP/trigger.h>
#include <trillBPP/reedsolomon.h>
#include <vector>
#include <time.h>


//#include <android/log.h>



using namespace std;



void MTFSK::plotData(vec plotVector)
{
    FILE* gnuplot_pipe = popen ("gnuplot -persistent", "w");
    // basic settings
    fprintf(gnuplot_pipe, "set title '%s'\n", "Plot");
    // fill it with data
    for(int j=0; j<plotVector.size(); j++){
        fprintf(gnuplot_pipe, "%d %f\n", j, plotVector[j]);
    }
    fprintf(gnuplot_pipe, "e\n");
    fflush(gnuplot_pipe);
}


//Blockwise demodulation
Array<vec> BPF_Blockwise(vec &input,int state,int fc, int ov_fact, int span, int phi,int micstate)
{

	int del_f;
	float T_b = float(ov_fact) / FS;
	del_f = (2 * (float(1) / (T_b)));
	vec data_block;
	int j=0;
	vec maintaps;
	cvec FFT_block;
	int fStart=0,fEnd=0;
	modulation mod;
	Array<vec> BPFout(8);
	vec elemout;
	int c=0;
	cvec FFT_18556 = fft(taps_fc_18556,8192,micstate);
		cvec FFT_17852 = fft(taps_fc_17852,8192,micstate);
		cvec FFT_18908 = fft(taps_fc_18908,8192,micstate);
		cvec FFT_18204 = fft(taps_fc_18204,8192,micstate);
		cvec FFT_19612 = fft(taps_fc_19612,8192,micstate);
		cvec FFT_19260 = fft(taps_fc_19260,8192,micstate);
		cvec FFT_19964 = fft(taps_fc_19964,8192,micstate);
		cvec FFT_17500 = fft(taps_fc_17500,8192,micstate);
	if(state == true)
	{
	while(j<input.size())
	{
		if(j+6936 > input.size())
		{
			data_block=input.get(j,input.size()-1);

		}
		else
		{
			data_block=input.get(j,j+6936-1);

		}
	  //  __android_log_write(ANDROID_LOG_ERROR, "JNI LOGS", "block fft started");

		FFT_block = fft(data_block,8192,micstate);
	   // __android_log_write(ANDROID_LOG_ERROR, "JNI LOGS", "block fft ended");
		for(int i=0;i<8;i++)
			{
				fStart=   (fc+i*del_f)-del_f/2;
				//cout << fStart << "fstart of i is"  << i << endl;
				fEnd  =   (fc+i*del_f)+del_f/2;
				switch(fStart)
								{
									case 18556-176:
										elemout = real(ifft(elem_mult(FFT_18556,FFT_block)));
										break;
									case 17852-176:
										elemout = real(ifft(elem_mult(FFT_17852,FFT_block)));
										break;
									case 18908-176:
										elemout = real(ifft(elem_mult(FFT_18908,FFT_block)));
										break;
									case 18204-176:
										elemout = real(ifft(elem_mult(FFT_18204,FFT_block)));
										break;
									case 19612-176:
										elemout = real(ifft(elem_mult(FFT_19612,FFT_block)));
										break;
									case 19260-176:
										elemout = real(ifft(elem_mult(FFT_19260,FFT_block)));
										break;
									case 19964-176:
										elemout = real(ifft(elem_mult(FFT_19964,FFT_block)));
										break;
									case 17500-176:
										elemout = real(ifft(elem_mult(FFT_17500,FFT_block)));
										break;
								}
				//TODO : should strip at end block;
				//elemout.del(0,delay_data-2);
				elemout = elemout.get(delay_data-1,delay_data-1+6935);


				//elemout = elemout.get(delay_data-1,elemout.size()-delay_data);
			BPFout(i) = concat(BPFout(i),elemout);
			}
		c++;
		//TODO : data parsing is not done properly:
		j=j+6936;
	}


	int nperseg = 256;
	double welchOut=0,maxWelchPower = 0;
	//cout << BPFout(0).size();
	//Plot1(BPFout(6));
	for(int i=0;i<8;i++)
	{
		mod.getWelchPSD(BPFout(i),welchOut,nperseg,FS,micstate);
		if(i == 0)
		{
			maxWelchPower = welchOut;
		}
		BPFout(i) *= sqrt(pow(10,(maxWelchPower-welchOut)/10));
	}


}
	else
	{
			FFT_block = fft(input,8192,micstate);
		for(int i=0;i<8;i++)
					{
						fStart=   (fc+i*del_f)-del_f/2;
						//cout << fStart << "fstart of i is"  << i << endl;
						fEnd  =   (fc+i*del_f)+del_f/2;
						//cout << fStart << endl;
						switch(fStart)
										{
											case 18556-176:
												elemout = real(ifft(elem_mult(FFT_18556,FFT_block)));
												break;
											case 17852-176:
												elemout = real(ifft(elem_mult(FFT_17852,FFT_block)));
												break;
											case 18908-176:
												elemout = real(ifft(elem_mult(FFT_18908,FFT_block)));
												break;
											case 18204-176:
												elemout = real(ifft(elem_mult(FFT_18204,FFT_block)));
												break;
											case 19612-176:
												elemout = real(ifft(elem_mult(FFT_19612,FFT_block)));
												break;
											case 19260-176:
												elemout = real(ifft(elem_mult(FFT_19260,FFT_block)));
												break;
											case 19964-176:
												elemout = real(ifft(elem_mult(FFT_19964,FFT_block)));
												break;
											case 17500-176:
												elemout = real(ifft(elem_mult(FFT_17500,FFT_block)));
												break;
										}

						//TODO : should strip at end block;
					//	cout << elemout.size() << endl;
						BPFout(i) = elemout.get(delay_data,elemout.size()-delay_data-1);
						//cout << BPFout(i).size() << "BPF size" << endl;
					}

						int nperseg = 256;
							double welchOut=0,maxWelchPower = 0;
							for(int i=0;i<8;i++)
							{
								mod.getWelchPSD(BPFout(i),welchOut,nperseg,FS,micstate);
								if(i == 0)
								{
									maxWelchPower = welchOut;
								}
								BPFout(i) *= sqrt(pow(10,(maxWelchPower-welchOut)/10));
							}
}
//	__android_log_write(ANDROID_LOG_ERROR, "JNI LOGS", "blocks BPF ended");
    return BPFout;
 }



//----- For returning multiple values from RLS
struct RLS_return{
	cvec d_obtained;
	cvec taps;
	cvec feedback_taps;
	cmat p_itir;
	cvec previous_detected_symbol;
};

//---- Others
cvec carrier_demod(vec conv_op, int fc, int phi);
cvec conv_builin(vec rrc_signal, const cvec demod_op_sine);

MTFSK::MTFSK(){
	rrc_wave = "-1.186425123934333565e-03,-1.319837694295986075e-03,-1.440990914655320299e-03,-1.548326236114834324e-03,-1.640388520565612537e-03,-1.715842530418102644e-03,-1.773488647310853938e-03,-1.812277658018902980e-03,-1.831324451007818174e-03,-1.829920473807568121e-03,-1.807544809579698540e-03,-1.763873740876002044e-03,-1.698788679577304490e-03,-1.612382354285104387e-03,-1.504963159931613963e-03,-1.377057588978238033e-03,-1.229410679180794729e-03,-1.062984429392285074e-03,-8.789541521229722468e-04,-6.787027494446193137e-04,-4.638129171662799593e-04,-2.360573008714037663e-04,2.613353767970246957e-06,2.500839951543562295e-04,5.040909244920835884e-04,7.622392208239981435e-04,1.022021816274449636e-03,1.280840088203831929e-03,1.536025818606934733e-03,1.784864355689659522e-03,2.024618798289331070e-03,2.252555010805471370e-03,2.465967264718985780e-03,2.662204292722916265e-03,2.838695533084155832e-03,2.992977335205924180e-03,3.122718892555979668e-03,3.225747666241455397e-03,3.300074061610118546e-03,3.343915121384109183e-03,3.355717002016227277e-03,3.334176005211992189e-03,3.278257943878478981e-03,3.187215631120665898e-03,3.060604292267624354e-03,2.898294713216514103e-03,2.700483953557142151e-03,2.467703469891670623e-03,2.200824513384174244e-03,1.901060685738914643e-03,1.569967559374908827e-03,1.209439290383346795e-03,8.217021767579692649e-04,4.093051391964047268e-04,-2.489287270549514806e-05,-4.777385200031448857e-04,-9.458027044820129089e-04,-1.425402107601659880e-03,-1.912623530310075164e-03,-2.403350899742240192e-03,-2.893294783384816465e-03,-3.378024226637316423e-03,-3.853000706045950526e-03,-4.313613968042940075e-03,-4.755219501998120789e-03,-5.173177376980834727e-03,-5.562892154025821448e-03,-5.919853570077324850e-03,-6.239677676312738348e-03,-6.518148102373066272e-03,-6.751257109285082811e-03,-6.935246087665788636e-03,-7.066645154252292234e-03,-7.142311498977084623e-03,-7.159466136767865009e-03,-7.115728723028779193e-03,-7.009150099370438543e-03,-6.838242246591149862e-03,-6.602005335140530111e-03,-6.299951579265565017e-03,-5.932125619671214413e-03,-5.499121180724830110e-03,-5.002093771874338742e-03,-4.442769228892021714e-03,-3.823447918637379924e-03,-3.147004461069368136e-03,-2.416882854031891618e-03,-1.637086919667695133e-03,-8.121660259512953825e-04,5.280392747436966693e-05,9.522442334823988733e-04,1.880101748972930458e-03,2.829881813746528297e-03,3.794685579542024945e-03,4.767251575129883082e-03,5.740001297054243834e-03,6.705088580225019614e-03,7.654452468359909841e-03,8.579873271550644831e-03,9.473031467252906318e-03,1.032556907204094238e-02,1.112915308478289525e-02,1.187554057772001286e-02,1.255664499049938439e-02,1.316460316372353515e-02,1.369184263322771426e-02,1.413114869424662336e-02,1.447573073602867293e-02,1.471928734241946882e-02,1.485606965256076190e-02,1.488094247820525608e-02,1.478944268027291425e-02,1.457783431718237196e-02,1.424316009116825821e-02,1.378328863619560085e-02,1.319695721213561164e-02,1.248380939446941396e-02,1.164442737681099446e-02,1.068035853482954441e-02,9.594135934522893261e-03,8.389292505038409981e-03,7.070368636123590921e-03,5.642913002560807738e-03,4.113476462323010195e-03,2.489598921383864155e-03,7.797891058151023647e-04,-1.006502769320962018e-03,-2.858919395066525631e-03,-4.766237738237559182e-03,-6.716417245932618588e-03,-8.696654695058725695e-03,-1.069344544756715976e-02,-1.269265082357233683e-02,-1.467957125733727018e-02,-1.663902485560382963e-02,-1.855543093427498499e-02,-2.041289806835518833e-02,-2.219531615164942878e-02,-2.388645192731577924e-02,-2.547004741825185242e-02,-2.692992065773826965e-02,-2.825006809600764945e-02,-2.941476803767426387e-02,-3.040868444844108467e-02,-3.121697045735993478e-02,-3.182537087329189579e-02,-3.222032303120043323e-02,-3.238905528557860930e-02,-3.231968247470604072e-02,-3.200129769054229495e-02,-3.142405970487892330e-02,-3.057927542280867300e-02,-2.945947675954167094e-02,-2.805849136597129978e-02,-2.637150666199415683e-02,-2.439512667423791858e-02,-2.212742121630520767e-02,-1.956796699465762276e-02,-1.671788027155931070e-02,-1.357984076774303335e-02,-1.002782619743188344e-02,-6.458519636041607642e-03,-2.488502348589549228e-03,1.742955972750607520e-03,6.225321628809160797e-03,1.094654873587930580e-02,1.589312076375667612e-02,2.105010128948667930e-02,2.640119374985615622e-02,3.192880991561319187e-02,3.761414675174392513e-02,4.343727127130397880e-02,4.937721293568247266e-02,5.541206310216708480e-02,6.151908097064513053e-02,6.767480543557512218e-02,7.385517220732411292e-02,8.003563552889825239e-02,8.619129378027581834e-02,9.229701823323543941e-02,9.832758419498337232e-02,1.042578037592040707e-01,1.100626593685500310e-01,1.157174373831704323e-01,1.211978608457319612e-01,1.264802206345656821e-01,1.315415042030887161e-01,1.363595211154684517e-01,1.409130246055663727e-01,1.451818284084086808e-01,1.491469181406501754e-01,1.527905565385578035e-01,1.560963818987254337e-01,1.590494991078147680e-01,1.616365626928239485e-01,1.638458513724120047e-01,1.656673336423409026e-01,1.670927239837711464e-01,1.681155293416034535e-01,1.687310855808932253e-01,1.689365836921746800e-01,1.687310855808908938e-01,1.681155293415988183e-01,1.670927239837641798e-01,1.656673336423316323e-01,1.638458513724005972e-01,1.616365626928103205e-01,1.590494991077990306e-01,1.560963818987075868e-01,1.527905565385380693e-01,1.491469181406284705e-01,1.451818284083851995e-01,1.409130246055413094e-01,1.363595211154417786e-01,1.315415042030605997e-01,1.264802206345362334e-01,1.211978608457013329e-01,1.157174373831387909e-01,1.100626593685174182e-01,1.042578037591706530e-01,9.832758419494934399e-02,9.229701823320088372e-02,8.619129378024092958e-02,8.003563552886316934e-02,7.385517220728894661e-02,6.767480543554002526e-02,6.151908097061024178e-02,5.541206310213252217e-02,4.937721293564840963e-02,4.343727127127050558e-02,3.761414675171119437e-02,3.192880991558128684e-02,2.640119374982522263e-02,2.105010128945674144e-02,1.589312076372791094e-02,1.094654873585175665e-02,6.225321628782963870e-03,1.742955972725937367e-03,-2.488502348612962357e-03,-6.458519636063564905e-03,-1.019167956666951896e-02,-1.357984076776187071e-02,-1.671788027157644629e-02,-1.956796699467316936e-02,-2.212742121631888076e-02,-2.439512667425008594e-02,-2.637150666200464844e-02,-2.805849136598015034e-02,-2.945947675954883882e-02,-3.057927542281426922e-02,-3.142405970488292705e-02,-3.200129769054484846e-02,-3.231968247470717176e-02,-3.238905528557835950e-02,-3.222032303119877483e-02,-3.182537087328904390e-02,-3.121697045735590675e-02,-3.040868444843591867e-02,-2.941476803766814377e-02,-2.825006809600057872e-02,-2.692992065773033503e-02,-2.547004741824317187e-02,-2.388645192730646030e-02,-2.219531615163955127e-02,-2.041289806834482509e-02,-1.855543093426423318e-02,-1.663902485559280373e-02,-1.467957125732603264e-02,-1.269265082356098827e-02,-1.069344544755578517e-02,-8.696654695047422237e-03,-6.716417245921431356e-03,-4.766237738226585322e-03,-2.858919395055816749e-03,-1.006502769310619813e-03,7.797891058250589185e-04,2.489598921393344419e-03,4.113476462331975246e-03,5.642913002569212473e-03,7.070368636131411921e-03,8.389292505045596940e-03,9.594135934529401943e-03,1.068035853483537134e-02,1.164442737681614139e-02,1.248380939447384617e-02,1.319695721213929793e-02,1.378328863619854988e-02,1.424316009117051682e-02,1.457783431718393148e-02,1.478944268027377294e-02,1.488094247820542955e-02,1.485606965256029699e-02,1.471928734241838635e-02,1.447573073602699371e-02,1.413114869424438903e-02,1.369184263322496473e-02,1.316460316372029989e-02,1.255664499049571198e-02,1.187554057771595187e-02,1.112915308477849252e-02,1.032556907203622740e-02,9.473031467247931131e-03,8.579873271545473620e-03,7.654452468354559086e-03,6.705088580219573449e-03,5.740001297048732617e-03,4.767251575124345844e-03,3.794685579536515897e-03,2.829881813741066086e-03,1.880101748967602905e-03,9.522442334772038101e-04,5.280392746932242122e-05,-8.121660259560991571e-04,-1.637086919672245530e-03,-2.416882854036198937e-03,-3.147004461073366240e-03,-3.823447918641065778e-03,-4.442769228895371465e-03,-5.002093771877339813e-03,-5.499121180727481635e-03,-5.932125619673490370e-03,-6.299951579267467142e-03,-6.602005335142065341e-03,-6.838242246592299983e-03,-7.009150099371227842e-03,-7.115728723029204200e-03,-7.159466136767936133e-03,-7.142311498976819210e-03,-7.066645154251702428e-03,-6.935246087664890917e-03,-6.751257109283894525e-03,-6.518148102371609971e-03,-6.239677676311030513e-03,-5.919853570075402777e-03,-5.562892154023695544e-03,-5.173177376978530147e-03,-4.755219501995680900e-03,-4.313613968040373552e-03,-3.853000706043288592e-03,-3.378024226634585535e-03,-2.893294783382034836e-03,-2.403350899739450756e-03,-1.912623530307293535e-03,-1.425402107598907524e-03,-9.458027044793177992e-04,-4.777385200005171049e-04,-2.489287270297851830e-05,4.093051391988121977e-04,8.217021767602571483e-04,1.209439290385474651e-03,1.569967559376871016e-03,1.901060685740714636e-03,2.200824513385784068e-03,2.467703469893093964e-03,2.700483953558364698e-03,2.898294713217536289e-03,3.060604292268447480e-03,3.187215631121283893e-03,3.278257943878894881e-03,3.334176005212212933e-03,3.355717002016254599e-03,3.343915121383950889e-03,3.300074061609781575e-03,3.225747666240949725e-03,3.122718892555317004e-03,2.992977335205114931e-03,2.838695533083213443e-03,2.662204292721854181e-03,2.465967264717819178e-03,2.252555010804206757e-03,2.024618798288000537e-03,1.784864355688277815e-03,1.536025818605500550e-03,1.280840088202369557e-03,1.022021816272973387e-03,7.622392208225240622e-04,5.040909244906260954e-04,2.500839951529202037e-04,2.613353766586968039e-06,-2.360573008727218309e-04,-4.638129171675385012e-04,-6.787027494458002267e-04,-8.789541521240652310e-04,-1.062984429393281889e-03,-1.229410679181687895e-03,-1.377057588979027115e-03,-1.504963159932283349e-03,-1.612382354285655595e-03,-1.698788679577735352e-03,-1.763873740876310608e-03,-1.807544809579885673e-03,-1.829920473807634474e-03,-1.831324451007767216e-03,-1.812277658018737097e-03,-1.773488647310579201e-03,-1.715842530417722956e-03,-1.640388520565132235e-03,-1.548326236114267070e-03,-1.440990914654670212e-03,-1.319837694295265731e-03,-1.186425123933543832e-03";
}
 cvec MTFSK::demodulate_MTFSK(vec input, int fc, int ov_fact, int span, int phi){
	cvec demod_op_sine = carrier_demod(input, fc,phi);
	cvec matched_filt_sine=conv_builin(rrc_wave, demod_op_sine);
	matched_filt_sine=matched_filt_sine.get( (10/2)*40, matched_filt_sine.size()-(10/2)*40 -1);
	//decoding//I changed this only for sync peak detection and that 40 is OV_FACT;10 is SPAN for sync
	return matched_filt_sine;
 }



 //Core demodulation function after peak detection and data start is known
 vec MTFSK::demodulate_MTFSK_8(vec input, bool tempstate,int micstate,int fc, int ov_fact, int span, int phi){
	int loop = input.size() / SAMPLES_PER_SYMBOL;
    Array<vec> BPFout(8);
    BPFout = BPF_Blockwise(input,true,fc,ov_fact,span,phi,micstate);
    vec answer(2*loop);
	double A[8];
    int del_f;
	float T_b = float(ov_fact) / FS;
	del_f = (2 * (float(1) / (T_b)));
	int tb=TB_DURATION;
    double s1,s2,x1,x2,y1,y2,z1,z2,a1,a2,b1,b2,c1,c2,d1,d2;
    for(int i=0;i<8;i++)
    {
    	A[i] = 2*PI*(Fdata+i*del_f);
    }
    double ts= 1.0/FS;
    for(int i=0;i<loop;i++)
    {
    	s1=s2=x1=x2=y1=y2=z1=z2=a1=a2=b1=b2=c1=c2=d1=d2=0;
    	for(int j=0;j<tb;j++)
    	{
    		s1 += (AMP_DATA * cos(A[0]*ts*j)) * (BPFout(0)((i *SAMPLES_PER_SYMBOL) + j));
    		s2 += (AMP_DATA * sin(A[0]*ts*j)) * (BPFout(0)((i *SAMPLES_PER_SYMBOL) + j));
    		x1 += (AMP_DATA * cos(A[1]*ts*j)) * (BPFout(1)((i *SAMPLES_PER_SYMBOL) + j));
    		x2 += (AMP_DATA * sin(A[1]*ts*j)) * (BPFout(1)((i *SAMPLES_PER_SYMBOL) + j));
    		y1 += (AMP_DATA * cos(A[2]*ts*j)) * (BPFout(2)((i *SAMPLES_PER_SYMBOL) + j));
    		y2 += (AMP_DATA * sin(A[2]*ts*j)) * (BPFout(2)((i *SAMPLES_PER_SYMBOL) + j));
    		z1 += (AMP_DATA * cos(A[3]*ts*j)) * (BPFout(3)((i *SAMPLES_PER_SYMBOL) + j));
    		z2 += (AMP_DATA * sin(A[3]*ts*j)) * (BPFout(3)((i *SAMPLES_PER_SYMBOL) + j));

    		a1 += (AMP_DATA * cos(A[4]*ts*j)) * (BPFout(4)((i *SAMPLES_PER_SYMBOL) + (j*1)+tb/2));
    		a2 += (AMP_DATA * sin(A[4]*ts*j)) * (BPFout(4)((i *SAMPLES_PER_SYMBOL) + (j*1)+tb/2));
    		b1 += (AMP_DATA * cos(A[5]*ts*j)) * (BPFout(5)((i *SAMPLES_PER_SYMBOL) + (j*1)+tb/2));
    		b2 += (AMP_DATA * sin(A[5]*ts*j)) * (BPFout(5)((i *SAMPLES_PER_SYMBOL) + (j*1)+tb/2));
    		c1 += (AMP_DATA * cos(A[6]*ts*j)) * (BPFout(6)((i *SAMPLES_PER_SYMBOL) + (j*1)+tb/2));
    		c2 += (AMP_DATA * sin(A[6]*ts*j)) * (BPFout(6)((i *SAMPLES_PER_SYMBOL) + (j*1)+tb/2));
    		d1 += (AMP_DATA * cos(A[7]*ts*j)) * (BPFout(7)((i *SAMPLES_PER_SYMBOL) + (j*1)+tb/2));
    		d2 += (AMP_DATA * sin(A[7]*ts*j)) * (BPFout(7)((i *SAMPLES_PER_SYMBOL) + (j*1)+tb/2));
    	}
    	double max_val_square = (s1*s1) + (s2*s2) + (a1*a1) + (a2*a2);
    	if((x1*x1) + (x2*x2) + (b1*b1) + (b2*b2) >= max_val_square)
    		max_val_square = (x1*x1) + (x2*x2) + (b1*b1) + (b2*b2);
    	if((y1*y1) + (y2*y2) + (c1*c1) + (c2*c2) >= max_val_square)
    	    max_val_square = (y1*y1) + (y2*y2) + (c1*c1) + (c2*c2);
    	if((z1*z1) + (z2*z2) + (d1*d1) + (d2*d2) >= max_val_square)
    	    max_val_square = (z1*z1) + (z2*z2) + (d1*d1) + (d2*d2);

    	if(max_val_square == (s1*s1) + (s2*s2) + (a1*a1) + (a2*a2))
    	{
    		answer[i*2+0] = 1;
    		answer[i*2+1] = 1;
    	}
    	else if(max_val_square == (x1*x1) + (x2*x2) + (b1*b1) + (b2*b2))
    	{
       		answer[i*2+0] = 1;
 	   		answer[i*2+1] = -1;
     	}
    	else if(max_val_square == (y1*y1) + (y2*y2) + (c1*c1) + (c2*c2))
		{
			answer[i*2+0] = -1;
			answer[i*2+1] = 1;
		}
    	else if(max_val_square == (z1*z1) + (z2*z2) + (d1*d1) + (d2*d2))
		{
			answer[i*2+0] = -1;
			answer[i*2+1] = -1;
		}
    }
	cout << answer.size() << endl;

    return answer;

 }

//Normalize data to sum squares
 vec Normalize(vec &input)
 {
	 vec result(input.size());
	 double S_sum=0;
	 for(int i=0;i<input.size();i++)
	 {
		 S_sum = S_sum + (input[i]*input[i]);
	 }
	 S_sum = S_sum/input.size();
	 S_sum = sqrt(S_sum);
	 for(int i=0;i<input.size();i++)
	 {
		 result[i] = input[i]/S_sum;
	 }
	 return result;
 }

// ----------------------------------------------------------------------
// -----------------------------------MAIN Function
// ----------------------------------------------------------------------



int MTFSK::demodulate_MTFSK_findpeak_New(vec input, int no_for_peak,int ChunkSize,double peak_ratio,int micstate,int index)
{
	int fStart =16000;
	int peak;
		  vec peak_detection= input.get(((no_for_peak)*ChunkSize + (index-1)*1024 - 300) , ((no_for_peak)*ChunkSize + (index)*1024 + 300));//Send only one chunk and 600 left to it and right to it.
	    //vec peak_detection= input.get(((no_for_peak)*ChunkSize - 600) , ((no_for_peak+1)*ChunkSize)+600);//Send only one chunk and 600 left to it and right to it.
		//vec peak_detection= input.get(0,no_for_peak);//Send first 20000 for peak detection
		modulation mod;
		int bpf1tapsize = 421;
	    cvec FFT_input = fft(peak_detection,peak_detection.size()+bpf1tapsize-1,micstate);
	    vec BPF_output = mod.filter_one_sided_N(FFT_input,fStart,micstate);
		double sum =0;
		double maxBPF = max(BPF_output);
		Correlate corr;
		vec corrOut = corr.baseband_corr(BPF_output,17000);		//Step : returns matched filter output(baseband)
		vector<int> ans;
		double corrMax = max(corrOut);
		double peakThreshold = peak_ratio*corrMax;
		for(int k=0;k<corrOut.size();k++)
		{
			if(corrOut[k] > peakThreshold)
			{
				if(corrOut[k] > corrOut[k-1] && corrOut[k] > corrOut[k+1])
				{
					ans.push_back(k +((no_for_peak)*ChunkSize + (index-1)*1024 - 300));
					break;
				}
			}
			if(corrOut[k] == corrMax)
				break;
		}
		peak = ans[0];
return peak;
}



int MTFSK::demodulate_MTFSK_findpeak(vec input, int no_for_peak,int ChunkSize,double peak_ratio,int micstate)
{
	int fStart =16000;
	int peak;
		vec peak_detection= input.get(((no_for_peak)*ChunkSize - 600) , ((no_for_peak+1)*ChunkSize)+600);//Send only one chunk and 600 left to it and right to it.
		//vec peak_detection= input.get(0,no_for_peak);//Send first 20000 for peak detection
		modulation mod;
		int bpf1tapsize = 842;
	    cvec FFT_input = fft(peak_detection,peak_detection.size()+bpf1tapsize-1,micstate);
		vec BPF_output = mod.filter_one_sided(FFT_input,fStart,micstate);
		Correlate corr;
		vec corrOut = corr.baseband_corr(BPF_output,17000);		//Step : returns matched filter output(baseband)
		vector<int > ans;
		double corrMax = max(corrOut);
		double peakThreshold = peak_ratio*corrMax;
		for(int k=0;k<corrOut.size();k++)
		{
			if(corrOut[k] > peakThreshold )
			{
				if(corrOut[k] > corrOut[k-1] && corrOut[k] > corrOut[k+1])
				{
					ans.push_back(k + (no_for_peak)*ChunkSize - 600);
					break;
				}
			}
			if(corrOut[k] == corrMax)
				break;
		}
		peak = ans[0];
return peak;
}


vec MTFSK::modulate_MTFSK(const char* string, int state)
{
	vec maindatabits;
	if(state == 1)
	{
	    int n = 10;
	    int ind=0;
	    bvec a(n*8+10);
	    for (int i = 0; i < n; i++)
	    {
	        char dummy = string[i];
	        int val = dummy;
	        vector<int>  bin;
	        while (val > 0)
	        {
	            if(val % 2 ==0)
	                bin.push_back(0);
	            else
	                bin.push_back(1);
	            val /= 2;
	        }
	        for(int j=0;j<(8-bin.size());j++)
	        {
	            a[ind] = 0;
	            ind++;
	        }
	        for(int j=bin.size()-1;j>=0;j--)
	        {
	            if(bin[j] == 0)
	                a[ind] = 0;
	            else
	                a[ind] = 1;
	            ind++;
	        }

	    }
	    int xo[8];
	    for(int i=0;i<8;i++)
	    {
	        if(a[i] ==0)
	        {
	            if(a[i+8] == 0)
	            xo[i] = 0;
	            else
	            xo[i] = 1;
	        }
	        else
	        {
	            if(a[i+8] == 0)
	            xo[i] = 1;
	            else
	            xo[i] = 0;
	        }
	    }

	    for(int i=16;i<80;i++)
	    {
	        if(xo[i%8] ==0)
	        {
	            if(a[i] == 0)
	                xo[i%8] = 0;
	            else
	                xo[i%8] = 1;
	        }
	        else
	        {
	            if(a[i] == 0)
	                xo[i%8] = 1;
	            else
	                xo[i%8] = 0;
	        }


	    }
	    for(int i=80;i<88;i++)
	    {
	        a[i] = xo[i%8];

	    }
	    for(int i=88;i<90;i++)
	    {
	        a[i]=1;
	    }
	    Reed_Solomon REED(5,11);
	    bvec encodedbits = REED.encode(a);
	    vec maindata(encodedbits.size());
	    for(int j=0;j<encodedbits.size();j++)
	    {
	    	if(encodedbits[j] == 0)
	    	maindata[j] = -1;
	    	else
	    	maindata[j] = 1;
	    }
	    maindatabits = maindata;

	}
	if(state == 0)
	{
		    int n = 4;
		    int ind=0;
		    bvec a(25);
		    for (int i = 0; i < 4; i++)
		    {
		        char dummy = string[i];
		        int val = dummy - '0';
		        vector<int>  bin;
		        while (val > 0)
		        {
		            if(val % 2 ==0)
		                bin.push_back(0);
		            else
		                bin.push_back(1);
		            val /= 2;
		        }
		        for(int j=0;j<(4-bin.size());j++)
		        {
		            a[ind] = 0;
		            ind++;
		        }
		        for(int j=bin.size()-1;j>=0;j--)
		        {
		            if(bin[j] == 0)
		                a[ind] = 0;
		            else
		                a[ind] = 1;
		            ind++;
		        }

		    }
		    int xo[8];
		    for(int i=0;i<8;i++)
		    {
		        if(a[i] ==0)
		        {
		            if(a[i+8] == 0)
		                xo[i] = 0;
		            else
		                xo[i] = 1;
		        }
		        else
		        {
		            if(a[i+8] == 0)
		                xo[i] = 1;
		            else
		                xo[i] = 0;
		        }
		    }

		    for(int i=16;i<24;i++)
		    {
		        a[i] = xo[i%8];

		    }
		    a[24] = 1;
		    bvec b;
		    Reed_Solomon REED(5,13);
		    b = REED.encode(a);

		    vec maindata(b.size() + 1);

		    for(int h=0;h<b.size();h++)
		    {

		        if(b[h] == 0)
		        {
		            maindata[h] = -1;
		        }
		        else
		        {
		            maindata[h] = 1;
		        }
		    }
		    maindata[b.size()] = 1;

	    maindatabits = maindata;
	}
return maindatabits;
}

string MTFSK::decode_bits(vec result)
{
	Reed_Solomon reed_solomon(5,13);
	    bvec reedencoded(result.size());
	    for(int i=0;i<result.size();i++)
	    {
	        if(result[i] == 1)
	        {
	            reedencoded[i] = 1;
	        }
	        if(result[i] == -1)
	        {
	            reedencoded[i] = 0;
	        }
	    }
	    bvec output1 = reed_solomon.decode(reedencoded);
	    string ans;
	    for(int i=0;i<output1.size();i++)
	    {
	        if(output1[i] == 1)
	            ans.push_back('1');
	        if(output1[i] == 0)
	            ans.push_back('0');
	    }
	    bvec xo(8);
	    for(int i=0;i<8;i++) {
	        if(output1[i] == output1[i+8])
	        {
	            xo[i] = 0;
	        }
	        else
	        {
	            xo[i] = 1;
	        }
	    }
	    bool checksum=true;
	    for(int i=0;i<8;i++)
	    {

	        if(xo[i]!=output1[16+i])
	        {
	            checksum = false;
	            break;
	        }
	    }

	    string answer;
	    if(output1[24] == 0)
	    {
	        checksum = false;
	    }
	    if(checksum == true) {
	    vec pho(4);
	        for(int s=0;s<4;s++)
	        {
	            int zero2 = 1;
	            int charsum = 0;
	            for(int z=3;z>=0;z--)
	            {

	                if(output1[4*s+z]  == 1)
	                {
	                    charsum = charsum + zero2;
	                }
	                zero2 = zero2*2;
	            }
	            char dummy = charsum + '0';
	            answer.push_back(dummy);
	        }
	    }

	    if(checksum == false)
	    {
		   for(int y=0;y<5;y++) 
	            answer.push_back('0');
	    }
return answer;
}
string MTFSK::decode_bytes(vec result)
{
	Reed_Solomon reed_solomon(5,11);
	    bvec reedencoded(result.size());
	    for(int i=0;i<result.size();i++)
	    {
	        if(result[i] == 1)
	        {
	            reedencoded[i] = 1;
	        }
	        if(result[i] == -1)
	        {
	            reedencoded[i] = 0;
	        }
	    }
	    bvec output1 = reed_solomon.decode(reedencoded);
	    string ans;
	    for(int i=0;i<output1.size();i++)
	    {
	        if(output1[i] == 1)
	            ans.push_back('1');
	        if(output1[i] == 0)
	            ans.push_back('0');
	    }
	    bvec xo(8);
	    for(int i=0;i<8;i++)
	    {
	        if(output1[i] == output1[i+8])
	        {
	            xo[i] = 0;
	        }
	        else
	        {
	            xo[i] = 1;
	        }
	    }
	    for(int i=16;i<80;i++)
	    {
	        if(output1[i] == xo[i%8])
	        {
	            xo[i%8] = 0;
	        }
	        else
	        {
	            xo[i%8] = 1;
	        }
	    }
	    bool checksum=true;
	    for(int i=0;i<8;i++)
	    {

	        if(xo[i]!=output1[80+i])
	        {
	            checksum = false;
	            break;
	        }
	    }
	    vec outputmain(output1.size());
	    for(int i=0;i<output1.size();i++)
	    {
	        if(output1[i] == 1)
	        {
	            outputmain[i] = 1;
	        }
	        if(output1[i] == 0)
	        {
	            outputmain[i] = -1;
	        }
	    }
	    string answer;
	    if(output1[88] != 1 && output1[89] != 1)
	    {
	        checksum = false;
	    }
	    if(checksum == true) {
	        vec pho(10);
	        for(int s=0;s<10;s++)
	        {
	            int zero2 = 1;
	            int charsum = 0;
	            for(int z=7;z>=0;z--)
	            {

	                if(output1[8*s+z]  == 1)
	                {
	                    charsum = charsum + zero2;
	                }
	                zero2 = zero2*2;
	            }
	            char dummy = charsum;

	            answer.push_back(dummy);
	        }
	    }

	    if(checksum == false)
	    {
		   for(int y=0;y<10;y++) 
	            answer.push_back('0');
	    }
return answer;
}

vec MTFSK::modulate_rawdata(vec maindatabits)
{
	vec symbol;
	    vec finaldata;
	    for(int i=0;i<maindatabits.size()/2;i++)
	    {
	        if(maindatabits[2*i] == 1)
	        {
	            if(maindatabits[2*i+1] == 1)
	            {
	                symbol = modMTFSK0;
	            }
	            if(maindatabits[2*i+1] == -1)
	            {
	                symbol = modMTFSK2;
	            }
	        }
	        else if(maindatabits[2*i] == -1)
	        {
	            if(maindatabits[2*i+1] == 1)
	            {
	                symbol = modMTFSK1;
	            }
	            if(maindatabits[2*i+1] == -1)
	            {
	                symbol = modMTFSK3;
	            }
	        }
	        finaldata = concat(finaldata,symbol);
	    }
	    finaldata= concat(syncWave,finaldata);
	return finaldata;
	}




// Demodulation init and peak logic function to detect the data start
vec MTFSK::trillDecoder_MTFSK_8(vec input ,int peak, int no_of_bits, int ChunkSize,double peak_ratio,int micstate){
	int distance_of_data=DIST_PEAK_DATA;
    vec tempdata ;
   // __android_log_write(ANDROID_LOG_ERROR, "JNI LOGS", "MTFSK started");

    int data_length =  (no_of_bits)*SAMPLES_PER_SYMBOL/2;
    vec datainput;
    datainput = input.get(peak+distance_of_data, peak+distance_of_data + data_length-1);
	vec Norm_data_RMS;
	Norm_data_RMS = Normalize(datainput);
	return demodulate_MTFSK_8(Norm_data_RMS,true,micstate);	//Gives final_bits
}

 // ----------------------------------------------------------------------
 // Other Functions not defined in class
 // ----------------------------------------------------------------------
cvec carrier_demod(vec input, int fc, int phi){
	int fs = FS;
	double ts = 1.0 / float(fs);
	//--full modulation
    int frames = input.size();  //complete data modulations
	double A = AMP_DATA;
	double np_arrange_upto = (frames - ts) * ts ;
	cvec carr_signal;
	double x;
	for( double i=0; i<np_arrange_upto; i+=ts){
		x = 2*M_PI*fc*i;
		double real_signal;
		double imag_signal;
		real_signal = A*sin(x + phi);
		imag_signal = A*cos(x + phi);
		std::complex<double> complex_signal = std::complex<double>( real_signal, imag_signal);
		carr_signal = concat(carr_signal, complex_signal);
	}
	cvec input_complex = to_cvec(input);
	if(input_complex.size() != carr_signal.size()){
		it_error("carr_signal and input signal length not same");
	}
	cvec modulated_signal = elem_mult(input_complex,carr_signal);	//Multiplying two cvecs elementwise. Function in vec.h
	return modulated_signal;
}

cvec conv_builin(vec rrc_signal,const cvec demod_op_sine){
	modulation M;
	cvec conv_op = M.convolve_freq_domain(demod_op_sine, rrc_signal);
	return conv_op;
}


